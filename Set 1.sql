Task 1:
DDL Operation (CREATE)
Write a SQL statement to create a new table named 'new_employees' that has the same structure as the 'employees' table.
Solution:
Create table New_Employees 
as 
Select*from Employees
Where 1=0;

Task 2:
DML Operation (INSERT)
Write a SQL statement to insert a new record into the 'employees' table. Use your own values for the data.
Solution:
Describe Employees;
Insert into Employees VALUES(207,'Leman','Huseynli','huseynlileman',070.818.9243,'01/01/2004','SQL DEV',150000,0.3,100,30);

Task 3:
SELECT Statement
Write a SQL statement to select all employees' first and last names and their department ids from the 'employees' table.
Solution:
Select first_name,last_name,department_id from Employees;

Task 4:
Single Function (Character)
Write a SQL statement to display the first name of all employees in lower case.
Solution:
Select lower(first_name) from Employees;

Task 5:
Aggregate Function
Write a SQL statement to find the average salary in the 'employees' table.
Solution:
Select round(avg(salary),3) from Employees;
