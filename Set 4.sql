Task 1:
DDL Operation (CREATE INDEX)
Write a SQL statement to create an index on the salary column in the 'employees' table.
Solution:
Create index idx_salary on Employees(salary);

Task 2:
DML Operation (INSERT with SELECT)
Write a SQL statement to insert into a new table 'high_salary_employees' all employees who have a salary greater than $10000.
Solution:
Create table high_salary_employees as
Select*from Employees
Where salary>10000;

Task 3:
SELECT Statement with ORDER BY clause
Write a SQL statement to select all employees' first and last names and their salaries, sorted by salary in descending order.
Solution:
Select first_name,last_name,salary from Employees order by salary desc;

Task 4:
Single Function (Date)
Write a SQL statement to calculate the number of years between the hire date and current date for all employees.
Solution:
Select 
   first_name,last_name,
  (Extract(year from sysdate)- Extract(year from hire_date)) as year_worked
From Employees;

Task 5:
Aggregate Function with GROUP BY
Write a SQL statement to find the total salary paid for each department.
Select department_id,sum(salary) as sum_salary from Employees group by department_id;
