Task 1:
SELECT with DISTINCT clause
Write a SQL statement to select all distinct job titles from the 'employees' table.
Solution:
Select distinct job_id from Employees;

Task 2:
SELECT with ALIAS
Write a SQL statement to display the 'employee_id' as 'Employee ID' and 'first_name' as 'First Name' from the 'employees' table.
Solution:
Select employee_id as "Employee ID", first_name as "First Name"
From employees;

Task 3:
SELECT with ORDER BY clause
Write a SQL statement to select all employees' first names, sorted in ascending order.
Solution:
Select first_name
From employees
Order by first_name asc;

Task 4:
SELECT with GROUP BY clause
Write a SQL statement to find the total salary for each job title.
Solution:
SELECT job_id, SUM(salary) AS total_salary
FROM employees
GROUP BY job_id;
Select*from Employees;

Task 5:
SELECT with LIMIT clause
Write a SQL statement to select the top 5 highest earning employees.
Solution:
SELECT first_name,last_name,salary 
FROM Employees
ORDER BY Salary
FETCH FIRST 5 ROWS ONLY;
