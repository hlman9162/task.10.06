Task 1:
SELECT with JOIN and ON clause
Write a SQL statement to display the employee's full name, department name, and location id.
Solution:
SELECT e.first_name,e.last_name,d.department_name,d.location_id
FROM employees e
INNER JOIN departments d
ON e.department_id=d.department_id;

Task 2:
SELECT with WHERE and LIKE clause
Write a SQL statement to find all employees whose first name starts with 'A'.
Solution:
SELECT*FROM employees WHERE first_name LIKE 'A%';

Task 3:
SELECT with WHERE and IN clause
Write a SQL statement to find all employees who work in department 10, 20 or 30.
Solution:
SELECT*FROM Employees WHERE department_id IN(10,20,30);

Task 4:
SELECT with WHERE and BETWEEN clause
Write a SQL statement to find all employees whose salary is between $5000 and $10000.
Solution:
SELECT*FROM employees WHERE salary BETWEEN 5000 AND 10000;

Task 5:
SELECT with Aggregate Function and HAVING clause
Write a SQL statement to find the job titles with more than 5 employees.
Solution:
SELECT job_id FROM employees
GROUP BY job_id HAVING count(employee_id)>5;
