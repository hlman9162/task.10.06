Task 1:
SELECT with JOIN and USING clause
Write a SQL statement to display the employee's full name and department name using the 'department_id' field.
Solution:
SELECT e.first_name,e.last_name,d.department_name
FROM employees e
JOIN departments d
USING(department_id);

Task 2:
SELECT with CASE statement
Write a SQL statement to categorize employees' salaries into 'Low', 'Medium' and 'High'.
Solution:
SELECT employee_id,
       first_name,
       last_name,
       salary,
   CASE 
      WHEN salary<5000 THEN 'Low'
      WHEN Salary>=5000 AND Salary<10000 THEN 'Medium'
      ELSE 'High'
   END AS Salary_Category
FROM employees;

Task 3:
SELECT with Subquery in SELECT clause
Write a SQL statement to display the department name and the highest salary in each department.
Solution:
SELECT department_name,
      (SELECT MAX(salary) FROM employees e
       WHERE d.department_id=e.department_id)
       AS highest_salary
FROM departments d;

Task 4:
SELECT with WHERE and NULL clause
Write a SQL statement to find all employees with no commission_pct.
Solution:
SELECT*FROM employees WHERE commission_pct IS NULL;

Task 5:
SELECT with GROUP BY and ROLLUP clause
Write a SQL statement to show the total salary, grouped by department and job title.
Solution:
SELECT job_id,
       department_id,
       SUM(salary) AS total_salary
FROM employees
GROUP BY ROLLUP(job_id,department_id);
