Task 1:
SELECT with multiple JOINS
Write a SQL statement to display the employee's name, their manager's name and the department name.
Solution:
--Manager name'in oldugu table movcud deyil,ona gore taskin sadece sintaksisinin nece yazildigini qeyd edecem.
SELECT e.first_name,e.last_name,m.manager_name,d.department_name
FROM employees e
JOIN departments d
ON e.manager_id=m.manager_id
ON e.department_id=d.department_id;

Task 2:
SELECT with WHERE and OR clause
Write a SQL statement to select all employees who are either 'IT_PROG' or 'SA_REP'.
Solution:
SELECT*FROM employees WHERE job_id='IT PROG' OR job_id='SA REP';

Task 3:
SELECT with Aggregate Function and GROUP BY clause
Write a SQL statement to find the average salary for each job title.
Solution:
SELECT job_id,AVG(salary) FROM employees
GROUP BY job_id;

Task 4:
SELECT with Subquery in WHERE clause
Write a SQL statement to select all employees who earn more than the average salary.
Solution:
SELECT*FROM employees WHERE salary>(SELECT AVG(salary) FROM employees);

Task 5:
SELECT with UNION clause
Write a SQL statement to combine the list of all department IDs in 'employees' and 'departments' table.
Solution:
SELECT department_id FROM employees
UNION
SELECT department_id FROM departments;
