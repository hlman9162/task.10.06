Task 1:
SELECT with Self JOIN
Write a SQL statement to find pairs of employees who have the same job title.
SELECT e1.employee_id,e1.job_id,e2.employee_id,e2.job_id
FROM employees e1
JOIN employees e2
ON e1.job_id=e2.job_id
WHERE e1.employee_id>e2.employee_id;

Task 2:
SELECT with Aggregate Function and WHERE clause
Write a SQL statement to find the total salary of employees whose department id is 30.
Solution:
SELECT SUM(salary) FROM employees WHERE department_id=30;

Task 3:
SELECT with UNION ALL clause
Write a SQL statement to combine the list of all department IDs in 'employees' and 'departments' table including duplicates.
Solution:
SELECT department_id FROM employees
UNION ALL
SELECT department_id FROM departments;

Task 4:SELECT with NOT IN clause
Write a SQL statement to select all employees who do not work in department 30, 50 or 80.
Solution:
SELECT*FROM employees WHERE department_id NOT IN(30,50,80);

Task 5:SELECT with Subquery in FROM clause
Write a SQL statement to select the highest paid employee from each department.
Solution:
SELECT department_id,MAX(salary) FROM employees GROUP BY department_id;
