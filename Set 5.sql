Task 1:
DDL Operation (ALTER TABLE MODIFY COLUMN)
Write a SQL statement to modify the data type of 'commission_pct' column in 'employees' table to NUMBER(3, 2).
Solution:
Alter table Employees Modify commission_pct number(3,2);

Task 2:
DML Operation (UPDATE with WHERE)
Write a SQL statement to update the department id to 20 for all employees whose last name is 'King'.
Solution:
Update Employees
Set department_id=20
Where Last_name='King';

Task 3:
Subquery in FROM clause
Write a SQL statement to find the maximum salary in each department.
Select department_id,max(salary) as max_salary
From Employees Group by department_id;

Task 4:
Set Operator (MINUS)
Write a SQL statement to find all department IDs that are in the 'departments' table but not in the 'employees' table.
Solution:
Select department_id from departments
Minus
Select department_id from employees;

Task 5:
Join (LEFT OUTER JOIN)
Write a SQL statement to display the department name for all employees, including those without a department.
Select d.department_name from employees e
Left outer join departments d 
On e.department_id=d.department_id;

