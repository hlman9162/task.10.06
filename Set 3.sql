Task 1:
DDL Operation (DROP)
Write a SQL statement to drop the 'new_employees' table.
Solution:
Drop table new_employees;

Task 2:
DML Operation (DELETE)
Write a SQL statement to delete all employees who are working in department id 10.
Solution:
Delete from employees where department_id=10;
--foreign key'e gore silmek mumkun olmadi

Task 3:
Subquery
Write a SQL statement to find the names of all employees who have a higher salary than the average salary.
Solution:
Select first_name,last_name,salary from employees where salary>(Select avg(salary) from employees);


Task 4:
Set Operator (UNION)
Write a SQL statement to display a list of all unique department IDs from both the 'employees' and 'departments' table.
Solution:
Select department_id from employees
union
Select department_id from departments;

Task 5:
Join (INNER JOIN)
Write a SQL statement to display the employee's full name and department name for all employees.
Solution:
Select e.first_name,e.last_name,d.department_name 
from employees e
inner join departments d
on e.department_id=d.department_id;
