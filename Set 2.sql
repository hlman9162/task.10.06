Task 1:
DDL Operation (ALTER)
Write a SQL statement to add a column 'middle_name' to the 'employees' table.
Solution:
Alter table employees add middle_name varchar2(100);

Task 2:
DML Operation (UPDATE)
Write a SQL statement to increase the salary of all employees by 10%.
Solution:
Update employees 
Set salary=salary*1.1;
commit;

Task 3:
SELECT Statement with WHERE clause
Write a SQL statement to select all employees who have a salary greater than $5000.
Solution:
Select*from employees Where salary>5000;

Task 4:
Single Function (Number)
Write a SQL statement to round the salary of all employees to the nearest whole number.
Solution:
Select employee_id,first_name,last_name,round(salary,-2) from employees;

Task 5:
**Conversion Function**
Write a SQL statement to convert the 'hire_date' of all employees to the format 'YYYY-MM-DD'.
Solution:
Update employees
Set hire_date=to_char(hire_date,'DD-MM-YYYY');

